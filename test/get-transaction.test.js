/**
 * @file Test case file for get transaction
 * @description In this file we have create test case for get transaction with filter 
 */

const { server } = require("../api/api");
const supertest = require('supertest')
const request = supertest(server)

/**
 * @description Here we have define multiple test cases with filter
 */
describe("This is transaction apis test", () => {

    test("This is get all transaction api testing", async () => {
        const response = await request.post("/api/get-all");
        expect(response.status).toBe(200)
        expect(response.body.success).toBe(true)
        expect(response.body.msg).toBe("Get data successfully")
    })

    test("This is get all transaction api testing with filter by status", async () => {
        const response = await request.post("/api/get-all").send({status: "A"});
        expect(response.status).toBe(200)
        expect(response.body.success).toBe(true)
        expect(response.body.msg).toBe("Get data successfully")
        expect(response.body.status.rows).toEqual(
            expect.arrayContaining([
              expect.objectContaining({status: "A"})
            ])
          );
    })
    
    test("This is get all transaction api testing with filter by currency code", async () => {
        const response = await request.post("/api/get-all").send({currencyCode: "USD"});
        expect(response.status).toBe(200)
        expect(response.body.success).toBe(true)
        expect(response.body.msg).toBe("Get data successfully")
        expect(response.body.status.rows).toEqual(
            expect.arrayContaining([
              expect.objectContaining({currencyCode: "USD"})
            ])
          );
    })
    
    test("This is get all transaction api testing with filter by currency code and status", async () => {
        const response = await request.post("/api/get-all").send({currencyCode: "USD", status: "A"});
        expect(response.status).toBe(200)
        expect(response.body.success).toBe(true)
        expect(response.body.msg).toBe("Get data successfully")
        expect(response.body.status.rows).toEqual(
            expect.arrayContaining([
              expect.objectContaining({currencyCode: "USD"}),
              expect.objectContaining({status: "A"})
            ])
          );
    })
})

