/**
 * @file Test case file for create
 * @description In this file we have create test case for upload file 
 */

const { server } = require("../api/api");
const supertest = require('supertest')
const request = supertest(server)

//File path that has to be upload 
const filePath = "C:/Users/samsh/Desktop/addresses.csv";

/**
 * @description This is a test case for upload file
 */
describe("This is upload transaction api test", () => {
    it("Upload api test", async () => {
        const response = await request.post("/api/upload").attach('file', filePath);
        expect(response.status).toBe(200)
        expect(response.body.success).toBe(true)
        expect(response.body.msg).toBe("Upload success")
    })
})

