
require('dotenv').config(); // load .env file

/**
 *@description Import all routes that are mapped to controller 
 */
const publicRoutes = require('./routes/publicRoutes');

const config = {
  publicRoutes,
  port: process.env.PORT || '3000',
};

module.exports = config;
