/**
 * @param {*} sequelize sequelize Connection
 * @param {*} Sequelize Sequelize Module for data-types
 * @description Here we have define schema for transaction model and return the same
 */

module.exports = (sequelize, Sequelize) => {
    const Transaction = sequelize.define("transaction", {
      transactionId: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true
      },
      amount: {
        type: Sequelize.FLOAT,
        allowNull: false
      },
      currencyCode: {
        type: Sequelize.STRING,
        allowNull: false
      },
      transactionDate: {
        type: Sequelize.DATE,
        allowNull: false
      },
      status: {
        type: Sequelize.STRING,
        allowNull: false
      },
      oldStatus: {
        type: Sequelize.STRING,
        allowNull: false
      }
    },
    {
        freezeTableName: true,
    });
  
    return Transaction;
  };