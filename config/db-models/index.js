/**
 * @file This file established DB connection and merge all models and export the connection object and all models.
 */

const dbConfig = require("../db.config.js");

const Sequelize = require("sequelize");

//Creating connection
const sequelize = new Sequelize(dbConfig.DB, dbConfig.USER, dbConfig.PASSWORD, {
  host: dbConfig.HOST,
  dialect: dbConfig.dialect,
  operatorsAliases: false,
  pool: {
    max: dbConfig.pool.max,
    min: dbConfig.pool.min,
    acquire: dbConfig.pool.acquire,
    idle: dbConfig.pool.idle
  }
});

//create a object to export
const db = {};
db.Sequelize = Sequelize;
db.sequelize = sequelize;
db.OP = Sequelize.Op;

//import all models and export from here.
db.transaction = require("./transaction.model.js")(sequelize, Sequelize);

module.exports = db;