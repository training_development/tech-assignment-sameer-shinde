/**
 * Mapped upload API routes to upload controller
 */
const uploadPubllic = {
  //Here we define 'request-method'  'API-endpoint' : 'controller-file.handle-function' 
  "POST /upload": "UploadController.upload",
  "POST /get-all": "UploadController.getAll",
};
module.exports = {
  uploadPubllic,
};
