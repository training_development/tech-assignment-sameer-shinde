/**
 * All routes get import here and pass as single object
 */

const { uploadPubllic } = require("./upload.routes.js");

/**
 * This is route Object
 */
const publicRoutes = {
  ...uploadPubllic
};

module.exports = publicRoutes;
