/**
 * @file This is logger file.
 * @description Here we created a logger that will be used in application to maintain all logs
 */

const { createLogger, format, transports, config } = require('winston');
const { combine, timestamp, json, errors, prettyPrint } = format;
 
/**
 * @description This is logger object that is used for application log
 */
module.exports.logger = new createLogger({
   format: combine(
       errors({ stack: true }),
       timestamp({
           format: 'YYYY-MM-DD HH:mm:ss'
       }),
       json(),
      //  prettyPrint()   //uncomment to print formated json
     ),  
   transports: [
       new transports.Console(),
       new transports.File({ filename: 'logs/combined.log' }),
       new transports.File({ level: "error", filename: 'logs/error.log' })
     ]
 });