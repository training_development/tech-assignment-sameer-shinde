const { server } = require("./api/api");
const config = require("./config")


// environment: development, staging, testing, production
const environment = process.env.NODE_ENV || "development";

//Listen server on specific port
server.listen(config.port, () => {
    if (
      environment !== "production" &&
      environment !== "development" &&
      environment !== "testing"
    ) {
      console.error(`NODE_ENV is set to ${environment}, but only production and development are valid.`);
      process.exit(1);
    }
    console.log(`auth server is running on ${config.port}`);
  });