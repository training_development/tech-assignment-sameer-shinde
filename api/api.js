/**
 * third party libraries
 */
const bodyParser = require("body-parser");
const express = require("express");
const helmet = require("helmet");
const http = require("http");
const mapRoutes = require("express-routes-mapper");

const cors = require("cors");

/**
 * server configuration
 */
const config = require("../config/");

//Import and sync DB
const db = require("../config/db-models/");
db.sequelize.sync();

/**
 * express application
 */
const app = express();
const server = http.Server(app);

/**
 * @description For routing API we have used express-routes-mapper (https://www.npmjs.com/package/express-routes-mapper) npm module that mapped route to contoller
 */
const mappedOpenRoutes = mapRoutes(config.publicRoutes, "api/controllers/");

// allow cross origin requests
// configure to only allow requests from certain origins
app.use(cors());

// secure express app
app.use(
  helmet({
    dnsPrefetchControl: false,
    frameguard: false,
    ieNoOpen: false,
  })
);

// parsing the request bodys
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// fill routes for express application
app.use("/api", mappedOpenRoutes);

//route not found handle
app.use((req, res) => {
  res.status(404).json({"statusCode": 404, "success": false, "message": "Route not found", "status": [], "error": {} })
});

module.exports = {
  server
}
