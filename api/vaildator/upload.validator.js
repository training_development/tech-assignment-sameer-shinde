/**
 * @file This file is for schema validation
 * @description In this file we have define JSON schema for validation
 */

const objectSchema = {
    "type" : "object",
    "properties" : {
        "transactionId" :  {
            "type" : "string",
            "isNotEmpty": true
        },
        "amount" : {
            "type" : "string",
            "isNotEmpty": true,
            "checkIsNumber": true
        },
        "currencyCode" : {
            "type" : "string",
            "isNotEmpty": true,
            "checkCurrenncy": true
        },  
        "transactionDate" : {
            "type" : "string",
            "isNotEmpty": true
        },
        "status" : {
            "type" : "string",
            "isNotEmpty": true,
           "enum": ["Approved", "Rejected", "Done", "Finished", "Failed"]
        }
    },
    "required" : ["transactionId", "amount", "currencyCode", "transactionDate", "status"],
    additionalProperties: false,
}
    
const uploadSchema = {
    "type": "array",
    "items" : objectSchema,
}
    
module.exports = {
    uploadSchema
}