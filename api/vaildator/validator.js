/**
 * @file schema validation file
 * @description Here we validate schema against data using AJV (https://www.npmjs.com/package/ajv) npm module and also we have created some custom validator as well
 */
const Ajv = require('ajv');
const schemaValidator = new Ajv();
const Util = require("../helper/util");

/**
 * @description This is custom validator that is to check empty string in object
 */
schemaValidator.addKeyword('isNotEmpty', {
    type: 'string',
    validate: function validate (schema, value, parentSchema, dataCxt) {
        validate.errors = [{keyword: 'required', message: `${dataCxt.instancePath.split("/").pop()} is required field.`, params: {keyword: 'required'}, invalidData: dataCxt.parentData}];
        return typeof value === 'string' && value.trim() !== ''
    },
    errors: true,
})

/**
 * @description This is custom validator that is to check the currency is valid ISO4217 format
 */
schemaValidator.addKeyword('checkCurrenncy', {
    type: 'string',
    validate: function validate (schema, value, parentSchema, dataCxt) {
        validate.errors = [{keyword: 'invalid', message: `${dataCxt.instancePath.split("/").pop()} is invaid, it must be in ISO4217 format.`, params: {keyword: 'invalid'}, invalidData: dataCxt.parentData}];
        return Util.currencyCode.includes(value || "")
    },
    errors: true,
})

/**
 * @description This is custom validator that is to check value is number or not
 */
schemaValidator.addKeyword('checkIsNumber', {
    type: 'string',
    validate: function validate (schema, value, parentSchema, dataCxt) {
        validate.errors = [{keyword: 'invalid', message: `${dataCxt.instancePath.split("/").pop()} is not valid number.`, params: {keyword: 'invalid'}, invalidData: dataCxt.parentData}];
        return !isNaN(value);
    },
    errors: true,
})

/**
 * @description This function validates data against define schema
 * @param {*} data that need to validate
 * @param {*} schema vaildation schema that validate against data
 * @returns retuns error if validation fails or else return null
 */
module.exports.validate = async (data, schema) => {
    try {
        const testSchemaValidator = await schemaValidator.compile(schema);
        const valid = testSchemaValidator(data);
        if(valid) {
            return null;
        } else {
            throw {
                statusCode: 400,
                error: testSchemaValidator.errors,
            }
        }
    } catch (error) {
        console.log(error);
        throw error
    }
}

