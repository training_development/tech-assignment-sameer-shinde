/**
 * @file This is Model file
 * @description This is file where are code logic are written and here we access database model
 */

const moment = require("moment");
const Util = require("../helper/util");
const fileHandle = require("../helper/fileHandler");
const db = require("../../config/db-models");
const { logger } = require("../../config/logger/logger");
const { uploadSchema } = require("../vaildator/upload.validator");
const { validate } = require("../vaildator/validator");
const Transaction = db.transaction;
const OP = db.OP;

/**
 * @param {*} req HTTP request
 * @returns return response with success/error
 * @description This function accept file that has to be upload 
 *              We pass the request to fileHandler to check the file and parse
 */
const upload = async (req) => {
  try {
    const requiredColumns = ["transactionId", "amount", "currencyCode", "transactionDate", "status"]
    //Pass request to handler to handle the file
    const fileResponse = await fileHandle.handleFile(req, requiredColumns);
    const fileData = fileResponse.data;

    //validate file data with AVJ validator
    await validate(fileData, uploadSchema);

    //Format data as per requirments
    const formatedFiledata = formatFileData(fileData);

    if(formatedFiledata.length) {
      //Save data to DB if vaild
      const dbResponse = await Transaction.bulkCreate(formatedFiledata);
      logger.info({message: "Data added successfully."});
      return {
        statusCode: 200,
        success: true,
        message: "Upload success",
        status: [],
        error: "",
      };
    } else {
      return {
        statusCode: 200,
        success: true,
        message: "Upload success",
        status: [],
        error: "",
      };
    }
  } catch (error) {
      logger.error({message: "Error while upload file", error});
      return {
      statusCode: error.statusCode || 503,
      success: false,
      message: "Upload failed",
      status: [],
      error: error,
    };
  }
};

/**
 * @description This function format file data and return the array
 * @param {*} fileData That needs to be formated. 
 * @returns Array of json 
 */
const formatFileData = fileData => {
  try {
    let finalJSON = [];
    for(let row of fileData) {
      //parse string value to float 
      row.amount = parseFloat(row.amount);
      //Formate string date to ISO or UTC
      row.transactionDate = moment(row.transactionDate, ["dd/MM/yyyy hh:mm:ss", "yyyy-MM-dd hh:mm:ss"]).format('YYYY-MM-DD HH:mm:ss');

      if(row.status == "Approved") {
        row.oldStatus = row.status;
        row.status = "A";
      } else if(row.status == "Rejected" || row.status == "Failed") {
        row.oldStatus = row.status;
        row.status = "R";
      } else if(row.status == "Finished" || row.status == "Done") {
        row.oldStatus = row.status;
        row.status = "D";
      }

      finalJSON.push(row);
    }//for end

    return finalJSON
  } catch (error) {
    throw error
  }
}

/**
 * 
 * @param {*} body body contain filter that should be apply to filter data 
 * @returns return filter data from DB
 * @description This function accept filter that apply while filtering data and return the same data
 */
const getAll = async (body) => {
  try {
    //create filter based on data pass in body
    let filter = {};
    if(body.startDate) {
      filter.transactionDate = {
        [OP.gte] : body.startDate
      }
      if(body.endDate) {
        filter.transactionDate = {
          ...filter.transactionDate,
          [OP.lte] : body.endDate
        };
      }
    } else if(body.startDate) {
      filter.transactionDate = {
        [OP.lte] : body.endDate
      };
    }
    delete body.endDate;
    delete body.startDate;

    Object.keys(body).forEach(key => {
      if(body[key]) {
        filter[key] = body[key];
      }
    });
    //Actual DB call to fetch data
    const transactionData = await Transaction.findAndCountAll({where: filter, raw: true});
    logger.info({message: "Data get successfully."})
    return {
      statusCode: 200,
      success: true,
      message: "Get data successfully",
      status: transactionData,
      error: "",
    };
  } catch (error) {
    logger.error({message: "Error while getting data", error});
    return {
      statusCode: error.statusCode || 400,
      success: false,
      message: "Get data failed",
      status: [],
      error: error,
    };
  }
}
module.exports = {
  upload,
  getAll
};
