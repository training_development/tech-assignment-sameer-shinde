/**
 * @file This is Controller file for Upload 
 * @description This file map user model to controller
 */

//Import Model
const Model = require("../models/upload");

/**
 * @description This is Controller function that handle API and pass to model
 * @returns function that handle API
 */
const UploadController = () => {

  /**
   * @param {*} req HTTP Request from express APP
   * @param {*} res HTTP Response from express APP
   * @description This function handle upload file API and pass it to the model
   * @returns returns the response received from Model
   * 
   * Request : form data with file
   * 
   * Response : {
                  "statusCode": 200,
                  "success": true,
                  "message": "Upload success",
                  "status": [],
                  "error": ""
                }
   * 
   * 
   */
  const upload = async (req, res) => {
    console.log("In upload Controller");
    const result = await Model.upload(req);
    return res.status(result.statusCode).json(result);
  };


  /**
   * @param {*} req HTTP Request from express APP
   * @param {*} res HTTP Response from express APP
   * @description This function handle get transaction API and pass it to the model
   * @returns returns the response received from Model
   * 
   * Request : {
                  "startDate":"2019-01-01 08:15:10",
                  "endDate":"2020-12-01 02:03:16",
                  "currencyCode": "USD", 
                  "status": "A"
                }
   *
   * Response : {
                  "statusCode": 200,
                  "success": true,
                  "message": "Get data successfuy",
                  "status": {
                      "count": 1,
                      "rows": [
                          {
                              "id": 1,
                              "transactionId": "Inv-003",
                              "amount": 1232.31,
                              "currencyCode": "USD",
                              "transactionDate": "2019-02-01T07:03:16.000Z",
                              "status": "A",
                              "createdAt": "2021-09-09T07:18:27.000Z",
                              "updatedAt": "2021-09-09T07:18:27.000Z"
                          }
                      ]
                  },
                  "error": ""
              }
   *
   */
  const getAll = async (req, res) => {
    console.log("In get-all transaction");
    const result = await Model.getAll(req.body);
    return res.status(result.statusCode).json(result);
  }

  //returns all the functions 
  return {
    upload,
    getAll
  };
};

module.exports = UploadController;
