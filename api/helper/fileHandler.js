/**
 * @file This file is for file handling
 * @description This is generic file that can be used for upload file handling.
 *              Here we used formidable (https://www.npmjs.com/package/formidable) module to read file request
 *              We also create seprate file handler based on file type such as csv/xml
 */

const formidable = require("formidable");
const csvHandler =  require("./csvHandler");
const xmlHandler =  require("./xmlHandler");
const fileSizeLimit = 2 * 1024 * 1024;

/**
 * 
 * @param {*} req HTTP Request
 * @param {*} columns requied columns that should be present into file
 * @returns return the data that is parsed by file handler
 */
module.exports.handleFile = async (req, columns) => {
    try {
        const form = new formidable.IncomingForm();
        return new Promise(async (resolve, reject) => {
            form.parse(req, async (err, fields, files) => {
              if (err) {
                return reject({
                  statusCode: 400,
                  success: false,
                  msg: "file upload error",
                });
              }
              if (!files || !files.file || !files.file.name) {
                  return reject({
                      statusCode: 400,
                      success: false,
                      msg: "Invalid file.",
                    });
              }
              let fileExtension = "";
              if(files?.file?.name) {
                  fileExtension = files.file.name.split(".").pop().toLowerCase();
                  if(fileExtension != "csv" && fileExtension != "xml") {
                    return reject({
                        statusCode: 400,
                        success: false,
                        msg: "Invalid file format.",
                      });
                  }
              }
              //Check file size (2MB max)
              if(files.file.size > fileSizeLimit) {
                return reject({
                  statusCode: 400,
                  success: false,
                  msg: "File size should not be greater than 2 MB.",
                });
              }
              
              try {
                let finalData = [];
                if(fileExtension == "csv") {
                  finalData = await csvHandler.handleCsv(files.file, columns);
                }
                if(fileExtension == "xml") {
                  finalData = await xmlHandler.handleXml(files.file, columns);
                }
                return resolve(finalData);
              } catch (error) {
                console.error("Error while upload csv", error);
                return reject(error);
              }
            });
          });
    } catch (error) {
        throw error;
    }
};

