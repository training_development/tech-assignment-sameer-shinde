/**
 *@file This file to handle XML file
 *@description This file handle XML file uploaded by user
 *             we have used xml2js (https://www.npmjs.com/package/xml2js) npm module to read XML file
 *             This returns array of json that parse.
 */

const xml2js = require('xml2js');
const fs = require("fs");

/**
 * 
 * @param {*} file XML file that are uploaded by user
 * @description This function parse xml file and return array of json
 * @returns array of json
 */
module.exports.handleXml = async (file) => {
    console.log("Inside XML file handler");
    return new Promise(async (resolve, reject) => {
        const parser = new xml2js.Parser({ attrkey: "ATTR" });
        let finalJSON = [];
        const fileData = fs.readFileSync(file.path);
        //Parse XML data to JSON
        parser.parseString(fileData, async (error, result) => {
          if(error) {
            return reject({
              statusCode: 404,
              success: false,
              msg: "Error while parsing file.",
            })
          }
          try {
            const formatedData = await formatTransactioData(result);
            resolve({
              statusCode: 200,
              success: true,
              data: formatedData,
            });            
          } catch (error) {
            reject(error);
          }
        });
    })
}

/**
 * 
 * @param {*} data Data that need to be formate
 * @description This function formate data as per requied that parse in above function
 * @returns formated data 
 */
const formatTransactioData = async (data) => {
  try {
    let arrayData = [];
    if(data.Transactions || data.transactions) {
      const tempData = data.Transactions || data.transactions;
      if(tempData.Transaction || tempData.transaction) {
        arrayData = tempData.Transaction || tempData.transaction;
      }
    }
    let finalJSON = [];
    //format data
    for(let obj of arrayData) {
      let newObj = {};
      
      if(obj?.ATTR?.id) {
        newObj.transactionId = obj.ATTR.id;
      }
      
      newObj.transactionDate = obj.TransactionDate?.[0] || "" ;
      newObj.status = obj.Status?.[0] || "";
      newObj.amount = 0;
      newObj.currencyCode = "";
      if(obj.PaymentDetails?.length) {
        newObj.amount = obj.PaymentDetails[0].Amount[0] || "";
        newObj.currencyCode = obj.PaymentDetails[0].CurrencyCode[0] || "";
      }
      finalJSON.push(newObj);
    }
    return finalJSON;
  } catch (error) {
    throw error;
  }
}