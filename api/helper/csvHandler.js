/**
 * @file This file handle csv upload part
 * @description Here we have used fast-csv module (https://www.npmjs.com/package/fast-csv) that allow us to read file in streams
 *              fs module to create stream
 *              we just parse the file and return the array of objects.
 */

const csv = require("fast-csv");
const fs = require("fs");

/**
 * 
 * @param {*} file CSV file that is uploaded by user
 * @param {*} columns required columns that should be present into CSV file.
 * @returns Array of objects.
 */
module.exports.handleCsv = async (file, columns) => {
    console.log("Inside CSV file handler");
    return new Promise(async (resolve, reject) => {
        let finalJSON = [];
        const stream = fs.createReadStream(file.path)
        stream.pipe(csv.parse({headers: true}))
            .on("error", (error) => {
                return reject({
                    statusCode: 404,
                    success: false,
                    msg: "Error while parsing file.",
                })
            })
            .on("data", (row) => {
                // columns.forEach(key => {
                //     if(!row[key]) {
                //         stream.destroy();
                //         return reject({
                //             statusCode: 404,
                //             success: false,
                //             msg: "Incomplete data.",
                //         })    
                //     }
                // });
                finalJSON.push(row);
            })
            .on("end", () => {
                return resolve({
                    statusCode: 200,
                    success: true,
                    data: finalJSON,
                })
            })
    })
}